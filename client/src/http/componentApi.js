import { $host } from './index'

//get group of components
export const fetchComponents = async (groupName) => {
    const {data} = await $host.get('http://localhost:7000/api/component?group='+groupName)
    return data
}
export const changeOneComponent = async (id, component) => {
    const {data} = await $host.put('http://localhost:7000/api/component/'+id, component)
    return data
}

// export const createComponent = async (component) => {
//     const {data} = await $host.post('http://localhost:7000/api/component')
//     return data
// }