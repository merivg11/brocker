import React, { useState, useEffect, useContext } from 'react';
// import './css/custom.css'
// import './css/index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Switch, Redirect } from "react-router-dom";
import { Routes } from "./routes";

import HomePageEditAdm from "./pages/Admin/HomePageEditAdm"
import S1TitleHomePageAdmin from "./pages/Admin/HomePageElem/S1TitleHomePageAdmin";
import CardsHomePageEdit from "./pages/Admin/HomePageElem/CardsHomePageEdit";
import S2TexteHomePageAdmin from "./pages/Admin/HomePageElem/S2TexteHomePageAdmin";

import Upgrade from "./pages/Admin/Upgrade";
import NotFoundPage from "./pages/Admin/NotFound";
import ServerError from "./pages/Admin/ServerError";

import FinPartnersPageAdmin from "./pages/Admin/FinPartnersPageAdmin";
import DocsEditAbout from "./pages/Admin/AboutPageElem/DocsEditAbout";
import AboutPageAdmin from "./pages/Admin/AboutPageAdmin";
import CustCredPageAdmin from "./pages/Admin/CustCredPageAdmin";
import FinCredPageAdmin from "./pages/Admin/FinCredPageAdmin";
import InsuranceProdPageAdmin from "./pages/Admin/InsuranceProdPageAdmin";
import CardProdPageAdmin from "./pages/Admin/CardProdPageAdmin";
import NewsPageAdmin from "./pages/Admin/NewsPageAdmin"
import BanksBenefPageAdmin from './pages/Admin/BanksBenefPageAdmin';
import RetailBenefPageAdmin from './pages/Admin/RetailBenefPageAdmin';
import TrPartnersPageAdmin from './pages/Admin/TrPartnersPageAdmin';
import PartnAboutPageAdmin from './pages/Admin/PartnAboutPageAdmin';
import ContactsPageAdmin from './pages/Admin/ContactsPageAdmin';


import Sidebar from './components/admin/Sidebar';



//main
import Home from './pages/Home'
import HomePrev from './pages/HomePrev'
import About from './pages/About'



const RouteWithLoader = ({ component: Component, ...rest }) => {
  return (
      <Route {...rest} render={props => (  <Component {...props} />  ) } />
  );
};

const RouteWithSidebar = ({ component: Component, ...rest }) => {


  return (
      <Route {...rest} render={props => (
          <>
            <Sidebar />

            <main className="content">
              <Component {...props} />
            </main>
          </>
      )}
      />
  );
};




function App() {
  return (
      <Switch>
        <RouteWithLoader exact path={Routes.NotFound.path} component={NotFoundPage} />
        <RouteWithLoader exact path={Routes.ServerError.path} component={ServerError} />
        {/* pages */}
        <RouteWithSidebar exact path={Routes.Upgrade.path} component={Upgrade} />

        {/* documentation */}
        <RouteWithSidebar exact path={Routes.HomePageEditAdm.path} component={HomePageEditAdm} />
        <RouteWithSidebar exact path={Routes.S1TitleHomePageAdmin.path} component={S1TitleHomePageAdmin} />
        <RouteWithSidebar exact path={Routes.CardsHomePageEdit.path} component={CardsHomePageEdit} />
        <RouteWithSidebar exact path={Routes.S2TexteHomePageAdmin.path} component={S2TexteHomePageAdmin} />
        <RouteWithSidebar exact path={Routes.DocsEditAbout.path} component={DocsEditAbout} />

        <RouteWithSidebar exact path={Routes.BanksBenefPageAdmin.path} component={BanksBenefPageAdmin} />
        <RouteWithSidebar exact path={Routes.RetailBenefPageAdmin.path} component={RetailBenefPageAdmin} />
        <RouteWithSidebar exact path={Routes.TrPartnersPageAdmin.path} component={TrPartnersPageAdmin} />
        <RouteWithSidebar exact path={Routes.PartnAboutPageAdmin.path} component={PartnAboutPageAdmin} />
        <RouteWithSidebar exact path={Routes.NewsPageAdmin.path} component={NewsPageAdmin} />
        <RouteWithSidebar exact path={Routes.ContactsPageAdmin.path} component={ContactsPageAdmin} />

        <RouteWithSidebar exact path={Routes.FinPartnersPageAdmin.path} component={FinPartnersPageAdmin} />
        <RouteWithSidebar exact path={Routes.AboutPageAdmin.path} component={AboutPageAdmin} />
        <RouteWithSidebar exact path={Routes.CustCredPageAdmin.path} component={CustCredPageAdmin} />
        <RouteWithSidebar exact path={Routes.FinCredPageAdmin.path} component={FinCredPageAdmin} />
        <RouteWithSidebar exact path={Routes.InsuranceProdPageAdmin.path} component={InsuranceProdPageAdmin} />
        <RouteWithSidebar exact path={Routes.CardProdPageAdmin.path} component={CardProdPageAdmin} />
        <RouteWithLoader exact path={Routes.Home.path} component={Home} />
        <RouteWithSidebar exact path={Routes.HomePrev.path} component={HomePrev} />
        <RouteWithLoader exact path={Routes.About.path} component={About} />

        <Redirect to={Routes.NotFound.path} />
      </Switch>
  );
}

export default App;
