const markdownParser = (text) => {
    const element = document.createElement('div')
    const toHTML = text
        .replace(/^### (.*$)/gim, '<h3 class="heading3">$1</h3>') // h3 tag
        .replace(/^##(.*$)/gim, '<h2 class="heading2 my-3">$1</h2>') // h2 tag
        .replace(/^# (.*$)/gim, '<h1>$1</h1>') // h1 tag
        .replace(/\*\*(.*)\*\*/gim, '<b>$1</b>') // bold text
        .replace(/\*(.*)\*/gim, '<i>$1</i>') // italic text
        .replace(/^!-/gim, '<ul class="list">')
        .replace(/^-!/gim, '</ul>')
        .replace(/^!\+/gim, '<ol class="list-ol">')// !
        .replace(/^\+!/gim, '</ol>')//!
        .replace(/^&(.*$)/gim, '<li>$1</li>')//  &
        .replace(/^%(.*$)/gim, '<p style="font-size: 12px">$1</p>')//  %
    element.innerHTML = toHTML.trim()
    return toHTML.trim(); // using trim method to remove whitespace
}


export default markdownParser