export const Routes = {
    // universal pages
    NotFound: { path: "/404" },
    ServerError: { path: "/500" },

    // main pages
    Home:{path:'/'},
    HomePrev:{path:'/hоme'},
    About:{path:'/about/'},



    // admin pages
    HomePageEditAdm: { path: "/admin/home-page" },
    S1TitleHomePageAdmin: { path: "/admin/home-page/first-section-title" },
    CardsHomePageEdit: { path: "/admin/home-page/cards" },
    S2TexteHomePageAdmin: { path: "/admin/home-page/second-section-left-text" },
    AboutPageAdmin: { path: "/admin/about-page" },
    DocsEditAbout: { path: "/admin/about-page/text" },
    FinPartnersPageAdmin: { path: "/admin/fin-partners-page" },
    CustCredPageAdmin: { path: "/admin/customers-credit-page" },
    FinCredPageAdmin: { path: "/admin/fin-credit-page" },
    InsuranceProdPageAdmin: { path: "/admin/insurance-products-page" },
    CardProdPageAdmin: { path: "/admin/card-products-page" },
    Upgrade: { path: "/admin/upgrade" },

    BanksBenefPageAdmin: { path: "/admin/BanksBenefPageAdmin" },
    RetailBenefPageAdmin: { path: "/admin/RetailBenefPageAdmin" },
    TrPartnersPageAdmin: { path: "/admin/TrPartnersPageAdmin" },
    PartnAboutPageAdmin: { path: "/admin/PartnAboutPageAdmin" },
    NewsPageAdmin: { path: "/admin/NewsPageAdmin" },
    ContactsPageAdmin: { path: "/admin/ContactsPageAdmin" },
};