import {makeAutoObservable} from 'mobx'

export default class HomeThirdSectionStore {
    constructor(){
        this._header = []
        this._items = []
        makeAutoObservable(this)
    }

    get header(){
        return this._header
    }
    setHeader(header){
        this._header = header
    }
    get items(){
        return this._items
    }
    setItems(items){
        this._items = items
    }
}