import {makeAutoObservable} from 'mobx'

export default class HomeCardsStore {
    constructor(){
        this._cards = []
        makeAutoObservable(this)
    }

    get cards(){
        return this._cards
    }
    setCards(cards){
        this._cards = cards
    }


}