import {makeAutoObservable} from 'mobx'

export default class AboutStore {
    constructor(){
        this._text = []
        makeAutoObservable(this)
    }

    get text(){
        return this._text
    }
    setItems(text){
        this._text = text
    }
}