import React from 'react';
import {Nav, Badge, Image, Button, Dropdown, Accordion, Navbar} from '@themesberg/react-bootstrap';
import { Link } from 'react-router-dom';
import { useLocation } from "react-router-dom";

const NavItem = (props) => {
    const location = useLocation();
    const { pathname } = location;

        const { title, link, external, target, classNames} = props;
        const navItemClassName = link === pathname ? "active" : "";
        const linkProps = external ? { href: link } : { as: Link, to: link };

        return (
            <Nav.Item className={navItemClassName}>
                <Nav.Link {...linkProps} target={target} className={classNames}>
                    <span className="sidebar-text">{title}</span>
                </Nav.Link>
            </Nav.Item>
        );
    };

export default NavItem;