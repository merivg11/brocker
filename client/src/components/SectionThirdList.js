import React from 'react';
import SectionThirdListItem from './SectionThirdListItem'
import {observer} from 'mobx-react-lite'
import { useContext, useEffect } from 'react';
import {Context} from '../index'
import {fetchComponents} from '../http/componentApi'

const SectionThirdList = observer(() => {
    const {thirdSectionContext} = useContext(Context)
    useEffect(()=>{
        fetchComponents('home-3s-items').then(data => thirdSectionContext.setItems(data))
    }, [])
    console.log(thirdSectionContext.items)
    return (
        <div className="section-third-list">
            {thirdSectionContext.items.map((item, index) =>
                <SectionThirdListItem text={item.text} key={item.id} textId={`home-3s-item-text-${index+1}`}/>
            )}
        </div>
    );
});

export default SectionThirdList;