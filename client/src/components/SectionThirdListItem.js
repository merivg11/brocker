import React from 'react';

const SectionThirdListItem = (props) => {
    return (
        <div className="section-third-list__item">
            <div className="section-third-list__icon">
                <span className="icon-wallet"></span>
            </div>
            <div className="section-third-list__message">
                {props.text}
            </div>
        </div>
    );
};

export default SectionThirdListItem;