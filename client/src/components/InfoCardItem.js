import React from 'react';
import '../css/index.css'

const InfoCardItem = (props) => {
    return (
        <div className="welcome-why-item">
            <div className="welcome-why-item__header" id={props.titleId}>
                {props.title}
            </div>
            <div className="welcome-why-item__sub" id={props.textId}>
                {props.text}
            </div>
        </div>
    );
};

export default InfoCardItem;