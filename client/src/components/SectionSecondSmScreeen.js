import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/index.css'
import '../css/custom.css'
import markdownParser from "../scripts/parser";

const SectionSecondSmScreen = () => {
    const inputValue = ' **Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции**'
    const inputElem = markdownParser(inputValue)
    return (
        <div className="section-second-sm-screen">
            <div className="container">
                <div className="section-second-message">
                    <section
                        dangerouslySetInnerHTML={{ __html: inputElem }}
                    />
                </div>
            </div>
        </div>
    );
};

export default SectionSecondSmScreen;