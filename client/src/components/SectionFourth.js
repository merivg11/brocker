import React from 'react';
import SectionFourthItem from "./SectionFourthItem";

const SectionFourth = () => {
    return (
        <div className="container">
            <div className="section-fourth">
                <div className="section-fourth-header">
                    <div className="section-fourth-header__message">
                        Финансовые партнеры
                    </div>
                    <div className="section-fourth-header__control_slider">
                        <div className="control_slider__btn">
                            <span className="icon-arrow-left"></span>
                        </div>
                        <div className="control_slider__btn">
                            <span className="icon-arrow-right"></span>
                        </div>
                    </div>
                </div>
                <div className="section-fourth-slider">
                    <SectionFourthItem />
                    <SectionFourthItem />
                    <SectionFourthItem />
                    <SectionFourthItem />
                    <SectionFourthItem />
                </div>
            </div>
        </div>
    );
};

export default SectionFourth;