import React, {useState} from 'react';
import {Button} from '@themesberg/react-bootstrap';
import {Link} from "react-router-dom";
import {Routes} from "../../routes";
import {Form} from '@themesberg/react-bootstrap';
import {changeOneComponent} from '../../http/componentApi'
import handleFormSubmit from '../../scripts/script'
import RestoreInputButton from '../../components/admin/RestoreInputButton'

const CardsEditForm = (props) => {

    const [title, setTitle] = useState(props.defTitle)
    const [text, setText] = useState(props.defText)

    const changeCard = (id) => {
        const formData = new FormData()
        formData.append('title', title)
        formData.append('text', text)
        changeOneComponent(id, formData)
        console.log(id)
    }

    //Save the value function - save it to localStorage as (ID, VALUE)
    function saveValue(e, id) {
        let val = e.target.value
        sessionStorage.setItem(id, val)
    }

    //get the saved value function - return the value of "v" from localStorage.
    function getSavedValue(v, def) {
        if (!sessionStorage.getItem(v)) {
            return def
        }
        return sessionStorage.getItem(v)
    }

    return (
        <div>
            <h1 className="h2">Карточка {props.num}</h1>
            <Form>
                <Form.Group className="mb-3">
                    <Form.Label>Section title</Form.Label>
                    <Form.Control as="textarea" rows="2" value={getSavedValue(props.titleId, title)}
                                  onChange={e => {
                                      setTitle(e.target.value)
                                      saveValue(e, props.titleId)
                                  }} id={props.titleId}/>
                    <RestoreInputButton StoreId={props.titleId}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Section text</Form.Label>
                    <Form.Control as="textarea" rows="3" value={getSavedValue(props.textId, text)}
                                  onChange={e => {
                                      setText(e.target.value)
                                      saveValue(e, props.textId)
                                  }}
                                  id={props.textId}/>
                    <RestoreInputButton StoreId={props.textId}/>
                </Form.Group>

                <Button as={Link} to={Routes.Home.path} variant="primary" className="my-3" onClick={() =>

                    changeCard(props.id)}>Применить</Button>

                <Button as={Link} to={Routes.Home.path} variant="primary" className="my-3 ms-3" onClick={() =>

                    handleFormSubmit([props.titleId, props.textId], [`card-title-id`, `card-text-id`])}>Превью</Button>
            </Form>
        </div>
    );
};

export default CardsEditForm;

