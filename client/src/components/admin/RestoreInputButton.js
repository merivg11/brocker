import React from 'react';
import {Button} from '@themesberg/react-bootstrap';

const RestoreInputButton = (props) => {
    return (
        <Button variant="secondary" size="sm" variant="outline-secondary" className="my-1" onClick={()=>sessionStorage.removeItem(props.StoreId)}>
            Restore
        </Button>
    );
};

export default RestoreInputButton;