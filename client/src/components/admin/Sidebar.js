import React, { useState } from "react";
import SimpleBar from 'simplebar-react';
import { CSSTransition } from 'react-transition-group';
import {Nav, Badge, Image, Button, Dropdown, Accordion, Navbar, Container, Offcanvas, Form, FormControl} from 'react-bootstrap';
import NavItem from '../../components/NavItem'

import { Routes } from "../../routes";


export default (props = {}) => {
    const [show, setShow] = useState(false);
    const showClass = show ? "show" : "";
    const [showDefault, setShowDefault] = useState(false);
    const handleClose = () => setShowDefault(false);
    const onCollapse = () => setShow(!show);

    return (
        <>
            <Navbar bg="dark" variant="dark" expand={false}>
                <Container fluid>
                    <Navbar.Toggle aria-controls="offcanvasNavbar" />
                    <Navbar.Offcanvas
                        id="offcanvasNavbar"
                        aria-labelledby="offcanvasNavbarLabel"
                        placement="start"
                    >
                        <Offcanvas.Header closeButton>
                            <Offcanvas.Title id="offcanvasNavbarLabel">Offcanvas</Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body>
                            <Nav className="justify-content-end flex-grow-1 pe-3">
                                                <NavItem title="Главная" link={Routes.HomePageEditAdm.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Финансовые партнёры" link={Routes.FinPartnersPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="О брокере" link={Routes.AboutPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Потребительский кредит" link={Routes.CustCredPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Денежный кредит" link={Routes.FinCredPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Страховые продукты" link={Routes.InsuranceProdPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Карточные продукты" link={Routes.CardProdPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Преимущества для банков" link={Routes.BanksBenefPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Преимущества для ритейла" link={Routes.RetailBenefPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Торговые партнёры" link={Routes.TrPartnersPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Партнеры о нас" link={Routes.PartnAboutPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Новости" link={Routes.NewsPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                                                <NavItem title="Контакты" link={Routes.ContactsPageAdmin.path} classNames={'d-flex justify-content-start align-items-center justify-content-between'}/>
                            </Nav>
                            <Form className="d-flex">
                                <FormControl
                                    type="search"
                                    placeholder="Search"
                                    className="me-2"
                                    aria-label="Search"
                                />
                                <Button variant="outline-success">Search</Button>
                            </Form>
                        </Offcanvas.Body>
                    </Navbar.Offcanvas>
                </Container>
            </Navbar>
        </>
    );
};
