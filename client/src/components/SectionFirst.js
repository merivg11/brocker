import React from 'react';
import firstSectionImage from '../images/1.png'
import '../css/index.css'

const SectionFirst = (props) => {
    return (
        <div className="welcome-wrapper">
            <div className="container">
                <div className="welcome">
                    <div className="welcome-main">
                        <div className="welcome-title">
                            <div className="welcome-title__header">
                                {props.title}
                            </div>
                            <div className="welcome-title__sub">
                                {props.text}
                            </div>
                            <div className="welcome-title__header_sm_screen">
                                {props.title}
                            </div>
                            <div className="welcome-title__sub_sm_screen">
                                {props.text}
                            </div>
                        </div>
                        <div className="welcome-images">
                            <img src={firstSectionImage}/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default SectionFirst;