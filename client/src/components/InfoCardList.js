import React from 'react';
import InfoCardItem from "./InfoCardItem";
import '../css/index.css'
import {observer} from 'mobx-react-lite'
import { useContext, useEffect } from 'react';
import {Context} from '../index'
import {fetchComponents} from '../http/componentApi'


const InfoCardList = observer(() => {
    const {cardGroup} = useContext(Context)
    useEffect(()=>{
        fetchComponents('card').then(data => cardGroup.setCards(data))
    }, [])
    return (
        <div className="welcome-why-wrapper">
            <div className="container" style={{position: 'relative', display: 'flex', justifyContent: 'center'}}>
                <div className="welcome-why">
                    {cardGroup.cards.map((card, index) =>
                            <InfoCardItem title={card.title} text={card.text} key={card.id} titleId={`card-title-id${index+1}`} textId={`card-text-id${index+1}`}/>
                        )}
                </div>
            </div>
        </div>

    );
})

export default InfoCardList;

