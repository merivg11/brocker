import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/index.css'
import '../css/custom.css'
import markdownParser from "../scripts/parser";

const SectionSecond = (props) => {
    const inputValue = ' **Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции Заголовок второй секции**'
    const inputElem = markdownParser(inputValue)
    return (
        <div className="section-second-wrapper">
            <div className="container">
                <div className="section-second">
                    <div className="section-second-info">
                        <div className="section-second-info-wrapper" style={{'fontSize': '40px'}}>
                            <section
                                    dangerouslySetInnerHTML={{ __html: inputElem }}
                                />
                        </div>
                    </div>
                    <div className="section-second-message">
                        {/*<ul>*/}
                        {/*    <li>*/}
                        {/*        «Финансовый Брокер» (от слова «брокер» – посредник, который помогает осуществлению*/}
                        {/*        сделок*/}
                        {/*    </li>*/}
                        {/*    <li>*/}
                        {/*        между заинтересованными сторонами) – бизнес-проект по эффективной организации*/}
                        {/*    </li>*/}
                        {/*    <li>*/}
                        {/*        процесса потребительского экспресс-кредитования непосредственно в точках продаж.*/}
                        {/*    </li>*/}
                        {/*</ul>*/}
                        <section
                            dangerouslySetInnerHTML={{ __html: inputElem }}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SectionSecond;