import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './css/custom.css'
import './css/index.css'
// import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { HashRouter } from "react-router-dom";

import "@fortawesome/fontawesome-free/css/all.css";
import "react-datetime/css/react-datetime.css";
import ScrollToTop from "./components/ScrollToTop";

import HomeCardsStore from "./store/HomeCardsStore"
import HomeSectionFirstStore from "./store/HomeSectionFirstStore"
import HomeThirdSectionStore from "./store/HomeThirdSectionStore"
import HomeSecondSectionStore from "./store/HomeSecondSectionStore"
import AboutStore from './store/AboutStore'

export const Context = createContext(null)

console.log(process.env.REACT_APP_API_URL)

ReactDOM.render(
    <Context.Provider value={{
        cardGroup: new HomeCardsStore(),
        sectionFirstContext: new HomeSectionFirstStore(),
        secondSectionContext: new HomeSecondSectionStore(),
        thirdSectionContext: new HomeThirdSectionStore(),
        AboutContext: new AboutStore()
    }}>
    <HashRouter>
        <ScrollToTop />
        <App />

    </HashRouter>
    </Context.Provider>,
    document.getElementById("root")
);

reportWebVitals();