import React from 'react';
import Header from "../components/Header";
import SectionFirst from "../components/SectionFirst";
import InfoCardList from "../components/InfoCardList";
import SectionSecond from "../components/SectionSecond";
import SectionSecondSmScreen from "../components/SectionSecondSmScreeen";
import SectionThird from "../components/SectionThird";
import SectionFourth from "../components/SectionFourth";
import Footer from "../components/Footer";
import {observer} from 'mobx-react-lite'
import { useContext, useEffect } from 'react';
import {Context} from '../index'
import {fetchComponents} from '../http/componentApi'
import '../css/custom.css'

const Home = observer( () => {
    const {sectionFirstContext} = useContext(Context)
    const {thirdSectionContext} = useContext(Context)
    useEffect(()=>{
        fetchComponents('header').then(data => sectionFirstContext.setItems(data))
        fetchComponents('home-3s-header').then(data => thirdSectionContext.setHeader(data))
    }, [])
    console.log(sectionFirstContext.items)
    return (
        <>
            <Header />
            {sectionFirstContext.items.map((item) =>
                <SectionFirst title={item.title} text={item.text} key={item.id} titleId={`first-section-title`} textId={`first-section-text`}/>
            )}
            <InfoCardList />
            <SectionSecond />
            <SectionSecondSmScreen />
            {thirdSectionContext.header.map((header) =>
                <SectionThird header={header.text} key={header.id} textId={`third-section-header`}/>
            )}
            < SectionFourth />
            <Footer />
        </>
    );
});

export default Home;