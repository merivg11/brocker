import React from 'react';
import {Row, Col, Card, Container, Button} from '@themesberg/react-bootstrap';
import {Link} from "react-router-dom";
import {Routes} from "../../routes";


 const HomePageEditAdm = () => {
     return (
         <Container className="px-0">
             <Row>
                 <Col xs={12} className="p-3">
                     <Card>
                         <Card.Body>
                             <article>
                                 <h1 className="h2" id="overview">Компоненты главной страницы </h1>


                                 <p>Выберите компонент для редактирования:</p>

                                 <Button as={Link} to={Routes.S1TitleHomePageAdmin.path} variant="primary"
                                         className="my-3 mx-2">Заголовок первой секции</Button>
                                 <Button as={Link} to={Routes.CardsHomePageEdit.path} variant="primary"
                                         className="my-3 mx-2">Карточки</Button>
                                 <Button as={Link} to={Routes.S2TexteHomePageAdmin.path} variant="primary"
                                         className="my-3 mx-2">Левый параграф второй секции</Button>
                                 {/*<Button as={Link} to={Routes.DocsEdit.path} variant="primary"*/}
                                 {/*        className="my-3 mx-2">Правый параграф второй секции</Button>*/}
                                 {/*<Button as={Link} to={Routes.DocsEdit.path} variant="primary"*/}
                                 {/*        className="my-3 mx-2">Заголовок третьей секции</Button>*/}
                                 {/*<Button as={Link} to={Routes.DocsEdit.path} variant="primary"*/}
                                 {/*        className="my-3 mx-2">Пункты третьей секции</Button>*/}
                             </article>
                         </Card.Body>
                     </Card>
                 </Col>
             </Row>
         </Container>
     )
 }
export default HomePageEditAdm