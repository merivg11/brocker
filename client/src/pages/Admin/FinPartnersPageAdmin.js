import React from 'react';
import {Button} from '@themesberg/react-bootstrap';
import {Link} from "react-router-dom";
import {Routes} from "../../routes";
import {Form} from '@themesberg/react-bootstrap';

import handleFormSubmit from '../../scripts/script'

const FinPartnersPageAdmin = () => {
    return (
        <div>
            <Form>
                <Form.Group className="mb-3">
                    <Form.Label>Section text</Form.Label>
                    <Form.Control as="textarea" rows="3" id={'text1'}/>
                </Form.Group>
                <Button as={Link} to={Routes.About.path} variant="primary" className="my-3 ms-3" onClick={()=>

                    handleFormSubmit(['text1'],['about-text'])}>Превью</Button>
            </Form>
        </div>
    );
};

export default FinPartnersPageAdmin;