import React from 'react';
import {Row, Col, Card, Container, Button} from '@themesberg/react-bootstrap';
import {Link, Redirect} from "react-router-dom";
import {Routes} from "../../../routes";
import {Form} from '@themesberg/react-bootstrap';
// import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// import {faRocket} from "@fortawesome/free-solid-svg-icons";
//
// import script from "../../script";
// import handleFormSubmit from "../../script";


const S1TitleHomePageAdmin = () => {

    return (
        <Container className="px-0">
            <Row>
                <Col xs={12} className="p-3">
                    <Card>
                        <Card.Body>
                            <h1 className="h2">Заголовок первой секции</h1>
                            <div style={{width: '600px'}}>
                                <textarea type="text" id="text1" style={{width: '600px'}}/>
                            </div>

                            <Button as={Link} to={Routes.Upgrade.path} variant="primary" className="my-3" /*onClick={

                                handleFormSubmit}*/>Применить</Button>

                            <Button as={Link} to={Routes.Home.path} variant="primary" className="my-3 ms-3" /*onClick={

                                handleFormSubmit}*/>Превью</Button>
                            <Button as={Link} to={Routes.HomePageEditAdm.path} variant="primary" className="my-3 ms-3" /*onClick={

                                handleFormSubmit}*/>Отмена</Button>





                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>)
}

export default S1TitleHomePageAdmin;
