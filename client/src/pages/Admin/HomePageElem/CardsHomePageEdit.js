import {React, useContext, useEffect } from 'react';
import {Row, Col, Card, Container, Button} from '@themesberg/react-bootstrap';
import {Link} from "react-router-dom";
import {Routes} from "../../../routes";
import {Context} from '../../../index'
import {fetchComponents} from '../../../http/componentApi'
import CardsEditForm from '../../../components/admin/CardsEditForm'
import {observer} from 'mobx-react-lite'

// import script from "../../script";
// import handleFormSubmit from "../../script";

const CardsHomePageEdit = observer(() => {

    const {cardGroup} = useContext(Context)
    useEffect(()=>{
        fetchComponents('card').then(data => cardGroup.setCards(data))
    }, [])

    return (
        <Container className="px-0">
            <Row>
                <Col xs={12} className="p-3">
                    <Card>
                        <Card.Body>
                            {cardGroup.cards.map((card, index) =>
                                    <CardsEditForm defTitle={card.title} defText={card.text} num={index+1} titleId={`card-title-input-${index+1}`} textId={`card-text-input-${index+1}`} id={card.id} key={card.id}/>
                                )}

                            <Button as={Link} to={Routes.HomePageEditAdm.path} variant="primary" className="my-3" /*onClick={

                                handleFormSubmit}*/>Отмена</Button>


                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>)
})

export default CardsHomePageEdit;