import React, { useContext } from 'react';
import {Row, Col, Card, Container, Button} from '@themesberg/react-bootstrap';
import {Link, Redirect} from "react-router-dom";
import {Routes} from "../../../routes";
import {Form} from '@themesberg/react-bootstrap';
// import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// import {faRocket} from "@fortawesome/free-solid-svg-icons";
//
// import script from "../../script";
// import handleFormSubmit from "../../script";
import {Context} from "../../../index"
import {observer} from 'mobx-react-lite'

const DocsEditAbout =observer( () => {
        const {AboutStore} = useContext(Context)
    return (
        <Container className="px-0">
            <div>

            </div>
            <Row>
                <Col xs={12} className="p-3">
                    <Card>
                        <Card.Body>
                            <h1 className="h2">Текст страницы</h1>
                            <div style={{width: '800px', height: '400px'}}>
                                <textarea type="text" id="text1" style={{width: '800px', height: '400px'}}/>
                            </div>

                            <Button as={Link} to={Routes.Upgrade.path} variant="primary" className="my-3" /*onClick={

                                handleFormSubmit}*/>Применить</Button>

                            <Button as={Link} to={Routes.About.path} variant="primary" className="my-3 ms-3" /*onClick={

                                handleFormSubmit}*/>Превью</Button>
                            <Button as={Link} to={Routes.AboutPageAdmin.path} variant="primary" className="my-3 ms-3" /*onClick={

                                handleFormSubmit}*/>Отмена</Button>





                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>)
})

export default DocsEditAbout;
