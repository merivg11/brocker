import React from 'react';
import '../css/contact.css'
import '../css/custom.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/index.css'
import Header from '../components/Header'
import Footer from '../components/Footer'
import markdownParser from "../scripts/parser";


const About = () => {

    const inputValue = localStorage.getItem('about-text1')
    const inputElem = markdownParser(inputValue)
    console.log(markdownParser(inputValue))
    return (
        <div>
            <Header />
            <main>
                <div className="breadcrumb_content_wrapper">
                    <div className="container h-100">
                        <div className="breadcrumb_content">
                            <div className="breadcrumb_content__header">
                                О брокере
                            </div>
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><a href="#">Главная</a></li>
                                    <li className="breadcrumb-item active" aria-current="page">О брокере</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>




                <div className="container">
                    <div className="content" dangerouslySetInnerHTML={{ __html: inputElem }}>
                    </div>
                </div>
            </main>
            <Footer />
        </div>
    );
};

export default About;