import React from 'react';
import Header from "../components/Header";
import SectionFirst from "../components/SectionFirst";
import InfoCardList from "../components/InfoCardList";
import SectionSecond from "../components/SectionSecond";
import SectionSecondSmScreen from "../components/SectionSecondSmScreeen";
import SectionThird from "../components/SectionThird";
import SectionFourth from "../components/SectionFourth";
import Footer from "../components/Footer";
import InfoCardItem from "../components/InfoCardItem";


const HomePrev = () => {
    return (
        <div>
            < Header />
            <SectionFirst />
            <div className="welcome-why-wrapper">
                <div className="container" style={{position: 'relative'}}>
                    <div className="welcome-why">
                        <div className="welcome-why-item">
                            <div className="welcome-why-item__header">
                                Заголовок 1
                            </div>
                            <div className="welcome-why-item__sub">
                                реализовывать концепцию лояльности, значительно подняв уровень сервиса в торговой точке.
                            </div>
                        </div>
                        <div className="welcome-why-item">
                            <div className="welcome-why-item__header">
                                Заголовок2
                            </div>
                            <div className="welcome-why-item__sub">
                                текст
                            </div>
                        </div>
                        <div className="welcome-why-item">
                            <div className="welcome-why-item__header">
                                Выгоднее
                            </div>
                            <div className="welcome-why-item__sub">
                                реализовывать концепцию лояльности, значительно подняв уровень сервиса в торговой точке.
                            </div>
                        </div>
                        <div className="welcome-why-item">
                            <div className="welcome-why-item__header">
                                Выгоднее
                            </div>
                            <div className="welcome-why-item__sub">
                                реализовывать концепцию лояльности, значительно подняв уровень сервиса в торговой точке.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <SectionSecond />
            <SectionSecondSmScreen />
            <SectionThird />
            < SectionFourth />
            <Footer />
        </div>
    );
};

export default HomePrev;