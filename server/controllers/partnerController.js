const {Partner, Component} = require('../models/models')
const ApiError = require('../error/ApiError');
const uuid = require("uuid");
const path = require("path");

class PartnerController {
    async create(req, res, next) {
        try{
            if(req.files){
                const {img} = req.files
                let fileName = uuid.v4() + ".jpg"
                img.mv(path.resolve(__dirname, '..', 'static', fileName))
                const partner = await Partner.create({img})
                return res.json(partner)
            }
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    async getAll(req, res) {
        const partners = await Partner.findAll()
        return res.json(partners)
    }

    async deleteOne(req, res) {
        const {id} = req.params
        const partner = await Partner.destroy(
            {
                where: {id}
            },
        )
        return res.json({message: 'deleted'})
    }

}

module.exports = new PartnerController()
