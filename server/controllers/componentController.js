const uuid = require('uuid')
const path = require('path');
const {Component} = require('../models/models')
const ApiError = require('../error/ApiError');

class ComponentController {
    async create(req, res, next) {
        try {
            let {title, text, group} = req.body
            if(req.files){
                const {img, icon} = req.files
                let fileName = uuid.v4() + ".jpg"
                let fileNameIcon = uuid.v4() + ".svg"
                img.mv(path.resolve(__dirname, '..', 'static', fileName))
                icon.mv(path.resolve(__dirname, '..', 'static', fileNameIcon))
                const component = await Component.create({title, text, group, img: fileName, icon: fileNameIcon});
                return component
            }

            const component = await Component.create({title, text, group});
            return res.json(component)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }

    }

    async getAll(req, res) {
        let {group} = req.query
        if(group){
            let components = await Component.findAll({where:{group}})
            return res.json(components)
        }

    }

    async getOne(req, res) {
        const {id} = req.params
        const component = await Component.findOne(
            {
                where: {id}
            },
        )
        return res.json(component)
    }

    async changeOne(req, res, next) {
        try {
            const {id} = req.params
            const {title, text} = req.body
            if(req.files){
                const {img, icon} = req.files
                let fileName = uuid.v4() + ".jpg"
                let fileNameIcon = uuid.v4() + ".svg"
                img.mv(path.resolve(__dirname, '..', 'static', fileName))
                icon.mv(path.resolve(__dirname, '..', 'static', fileNameIcon))
                await Component.update( {title, text, img, icon}, {where: {id} }).then(() =>{
                    Component.findOne(
                        {
                            where: {id}
                        }).then((component) =>{
                        return res.json(component)
                    })
                })
            }

            await Component.update( {text, title}, {where: {id} }).then(() =>{
                Component.findOne(
                    {
                        where: {id}
                    }).then((component) =>{
                    return res.json(component)
                })
            })
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }


    }

}

module.exports = new ComponentController()
